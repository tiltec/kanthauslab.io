# Kanthaus

Informative website for the Kanthaus Project, all documents and meetings are also stored here in the contents folder

## How to add new documents
To add new documents to this repo (repository) you will need a gitlab account with the correct permissions to add files to this repository

There 2 easy options to add documents.
For either option you will need to have the correct access to this re

1>
- clone this repo:
  ```
  git clone https://gitlab.com/kanthaus/kanthaus.gitlab.io.git```
- edit the markdown (.md) files in the content folder
- git add . (add all changes)
- git commit -m "your changes eg. added new meeting notes"
- git push (you will be asked for your login details)

2>
- login to an account which has access to this repository
- view the source files
- enter the contents folder
- use gitlab's online code editor to change the content

## How to add multilingual pages/posts
to make an existing .md file multi-lingual simply add the language code extension before .md
For example for:
contact.md
add german translation:
contact.de.md
add english translation:
contact.en.md

