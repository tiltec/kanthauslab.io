---
title: "Visiting"
description: " "
weight: 1
draft: false
---

# Information for Visitors
So you want to come to Kanthaus? Great! Read on...

## General
- Be welcome for a week. To be welcome for longer, chat with a [Volunteer](../volunteers.yml) or [Member](../members.yml) once you get here.
- There is no cost for staying here. If you have money that you want to give, you can.
- We have [Collective Agreements](../collectiveagreements) which you accept by coming here. In short: smoke outside, drink responsibly, talk to us before bringing animals and come to the coordination meetings (Mondays, 10:00 in the office.)
- We have a [Constitution](../constitution) which you accept by coming here. In short: act, acknowledge and try to resolve disagreements, and if a situation is too complex or otherwise unresolvable use [score voting](../constitution/#8c-score-voting).
- The address is [Kantraße 20, Wurzen 04848](https://www.openstreetmap.org/search?query=20%20kantstrasse%20wurzen#map=19/51.36711/12.74075&layers=N).
- Contact us before you arrive if possible, via email: dmwebb--AT--gmail.com (Dougs email - temporary,) phone: 03425 8527995 or slack: #kanthaus on https://yunity.slack.com.

## Safety
- There are no smoke detectors or fire extinguishers: don't leave naked flames unattended.
- There may be sharp objects sticking out of the flooring: take care if walking barefoot.
- There is [Hylotox 59 pesticide](https://de.wikipedia.org/wiki/Hylotox) (Lindane and DDT) in the wood of the attic: please read the [advice](../../signs/attic) before going there.

## Resources
- Water: drinkable water comes out the taps. You can shower cold and/or test the solar shower.
- Food: some long-life food, a regular supply of bread and an irregular supply of fresh food. You are warmly invited to bring fruit and vegetables should you have an abundance.
- Electricity: 6 solar panels + 2 car batteries. We can charge phones, laptops* and rechargeable batteries. With minimal lighting, you may wish to bring a torch.
- Internet: name, "wuppdays", password, "dragonlove". (Download, ~25 Mbps, upload, ~5 Mbps.)
- Cooking: camping gas (limited) and alcohol (limited) burners in the kitchen (well-equipped.) A charcoal (plentiful) barbecue in the garden.
- Clothing: some T-shirts, trousers and underwear. Loads of rubber boots.
- Sleeping: 6 mattresses, sofas, camping mattresses, blankets, sleeping bags, duvets and sheets.
- Other: a guitar, a chess set, a [set](https://en.wikipedia.org/wiki/Set_(game)) set, various books, a shisha, basic hand tools, some electronics stuff...

Kanthaus eagerly awaits you!

_*We can charge Thinkpads easily, both old (barrel-type) and new (rectangle-type) sockets. We can charge other laptops, if: (1) you bring the corresponding 12 V DC (car) charger, or (2) your laptop takes 20 V DC and you're happy for us to perform some minor surgery on your regular charger. Laptops can also be charged at the nearby library during [opening hours](http://www.kultur-in-wurzen.de/index.php/startseite-bibliothek/bibliothek-angebot)._
