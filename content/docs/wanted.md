---
title: "Wish List"
description: " "
weight: 4
draft: false
---

# Wanted
- a projector (see Matthias)
- a piano (see Nick)
- Risk (the board game, 'Risiko')
- Monopoly (the board game)
- dice (at least 20, for the game Perudo)
- a barbell (langhantel), dumbbells, weights and mat
- a pinboard
- double-sided tape
